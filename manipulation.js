function ManipulateElements(){

}

ManipulateElements.prototype.addItemsToList = function(){
  var list = "";
  for(var i = 0; i < 5; i++){
    list += "<li>List item " + (i+8) + "</li>"
  }
  $("#myList").append(list);
}

ManipulateElements.prototype.removeEvenItems = function(){
  $("#myList li:even").remove();
}

ManipulateElements.prototype.appendItems = function(){
  $("div.module")
    .last()
    .append("<h2></h2>")
    .append("<p></p>");
}

ManipulateElements.prototype.addOptionToSelect = function(){
  var wed = $("<option></option>");
  wed
  .val("Wednesday")
  .text("Wednesday");

  $("#specials select")
  .append(wed);
}

ManipulateElements.prototype.addImageToDiv = function(){
  var lastDivModule = $("div.module").last();
  var newDivModule = $("<div></div>");
  newDivModule.addClass("module");
  var copiedImage = $("#slideshow img").first();
  newDivModule.append(copiedImage.clone())
  lastDivModule.after(newDivModule);
}

$(document).ready(function(){
  var manipulateElements = new ManipulateElements();
  // 1
  manipulateElements.addItemsToList();
  // 2
  manipulateElements.removeEvenItems();
  // 3
  manipulateElements.appendItems();
  // 4
  manipulateElements.addOptionToSelect();
  // 5
  manipulateElements.addImageToDiv();
});